from django.urls import path
from proveedor import views

urlpatterns = [
    path("",views.ListProveedorAPIView.as_view(),name="proveedor_list"),
    path("create/", views.CreateProveedorAPIView.as_view(),name="proveedor_crear"),
    path("update/<int:pk>/",views.UpdateProveedorAPIView.as_view(),name="update_proveedor"),
    path("delete/<int:pk>/",views.DeleteProveedorAPIView.as_view(),name="delete_proveedor"),

    path("user",views.ListUsuarioAPIView.as_view(),name="usuario_list"),
    path("Usercreate/", views.CreateUsuarioAPIView.as_view(),name="usuario_create"),
    path("Userupdate/<int:pk>/",views.UpdateUsuarioAPIView.as_view(),name="update_usuario"),
    path("Userdelete/<int:pk>/",views.DeleteUsuarioAPIView.as_view(),name="delete_usuario")
]