from django.db import models


class Usuario(models.Model):
    usuario = models.CharField(max_length=200, unique=True)
    cedula  = models.CharField(max_length=200)
    nombre  = models.CharField(max_length=200)
    correo  = models.CharField(max_length=200)
    fecha_Nacimiento  = models.CharField(max_length=200)
    celular  = models.CharField(max_length=10)
    ciudad  = models.CharField(max_length=20)
    direccion  = models.CharField(max_length=50)
    clave  = models.CharField(max_length=200)

    def __str___(self):
        return self.title