from rest_framework import serializers
from proveedor.usuarioModel import Usuario#, Proveedor

class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = "__all__"